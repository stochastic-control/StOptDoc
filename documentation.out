\BOOKMARK [-1][-]{part.1}{I Introduction}{}% 1
\BOOKMARK [0][-]{chapter.1}{General context}{part.1}% 2
\BOOKMARK [0][-]{chapter.2}{General mathematical setting}{part.1}% 3
\BOOKMARK [-1][-]{part.2}{II Useful tools for stochastic control}{}% 4
\BOOKMARK [0][-]{chapter.3}{The grids and their interpolators}{part.2}% 5
\BOOKMARK [1][-]{section.3.1}{Linear grids}{chapter.3}% 6
\BOOKMARK [2][-]{subsection.3.1.1}{Definition and C++ API}{section.3.1}% 7
\BOOKMARK [2][-]{subsection.3.1.2}{The python API}{section.3.1}% 8
\BOOKMARK [1][-]{section.3.2}{Legendre grids}{chapter.3}% 9
\BOOKMARK [2][-]{subsection.3.2.1}{Approximation of a function in 1 dimension}{section.3.2}% 10
\BOOKMARK [2][-]{subsection.3.2.2}{Extension in dimension d}{section.3.2}% 11
\BOOKMARK [2][-]{subsection.3.2.3}{Troncature}{section.3.2}% 12
\BOOKMARK [2][-]{subsection.3.2.4}{The C++ API}{section.3.2}% 13
\BOOKMARK [2][-]{subsection.3.2.5}{The python API}{section.3.2}% 14
\BOOKMARK [1][-]{section.3.3}{Sparse grids}{chapter.3}% 15
\BOOKMARK [2][-]{subsection.3.3.1}{The linear sparse grid method}{section.3.3}% 16
\BOOKMARK [1][-]{section.3.4}{High order sparse grid methods}{chapter.3}% 17
\BOOKMARK [1][-]{section.3.5}{Anisotropy}{chapter.3}% 18
\BOOKMARK [1][-]{section.3.6}{Adaptation}{chapter.3}% 19
\BOOKMARK [1][-]{section.3.7}{C++ API}{chapter.3}% 20
\BOOKMARK [1][-]{section.3.8}{Python API}{chapter.3}% 21
\BOOKMARK [0][-]{chapter.4}{Introducing the regression resolution}{part.2}% 22
\BOOKMARK [1][-]{section.4.1}{C++ global API}{chapter.4}% 23
\BOOKMARK [1][-]{section.4.2}{Adapted local polynomial basis with same probability}{chapter.4}% 24
\BOOKMARK [2][-]{subsection.4.2.1}{Description of the method}{section.4.2}% 25
\BOOKMARK [2][-]{subsection.4.2.2}{C++ API}{section.4.2}% 26
\BOOKMARK [2][-]{subsection.4.2.3}{Python API}{section.4.2}% 27
\BOOKMARK [1][-]{section.4.3}{Adapted local basis by K-Means clustering methods}{chapter.4}% 28
\BOOKMARK [2][-]{subsection.4.3.1}{C++ API}{section.4.3}% 29
\BOOKMARK [2][-]{subsection.4.3.2}{Python api}{section.4.3}% 30
\BOOKMARK [1][-]{section.4.4}{Local polynomial basis with meshes of same size}{chapter.4}% 31
\BOOKMARK [1][-]{section.4.5}{C++ API}{chapter.4}% 32
\BOOKMARK [2][-]{subsection.4.5.1}{The constant per cell approximation}{section.4.5}% 33
\BOOKMARK [2][-]{subsection.4.5.2}{The linear per cell approximation}{section.4.5}% 34
\BOOKMARK [2][-]{subsection.4.5.3}{An example in the linear case}{section.4.5}% 35
\BOOKMARK [2][-]{subsection.4.5.4}{Python API}{section.4.5}% 36
\BOOKMARK [1][-]{section.4.6}{Sparse grid regressor}{chapter.4}% 37
\BOOKMARK [2][-]{subsection.4.6.1}{C++ API}{section.4.6}% 38
\BOOKMARK [2][-]{subsection.4.6.2}{Python API}{section.4.6}% 39
\BOOKMARK [1][-]{section.4.7}{Global polynomial basis}{chapter.4}% 40
\BOOKMARK [2][-]{subsection.4.7.1}{Description of the method}{section.4.7}% 41
\BOOKMARK [2][-]{subsection.4.7.2}{C++ API}{section.4.7}% 42
\BOOKMARK [2][-]{subsection.4.7.3}{Python API}{section.4.7}% 43
\BOOKMARK [1][-]{section.4.8}{Kernel regression}{chapter.4}% 44
\BOOKMARK [2][-]{subsection.4.8.1}{The univariate case}{section.4.8}% 45
\BOOKMARK [2][-]{subsection.4.8.2}{The multivariate case}{section.4.8}% 46
\BOOKMARK [2][-]{subsection.4.8.3}{C++ API}{section.4.8}% 47
\BOOKMARK [2][-]{subsection.4.8.4}{Python API}{section.4.8}% 48
\BOOKMARK [0][-]{chapter.5}{Calculating conditional expectation by trees}{part.2}% 49
\BOOKMARK [1][-]{subsection.5.0.1}{C++ API}{chapter.5}% 50
\BOOKMARK [0][-]{chapter.6}{Continuation values objects and similar ones}{part.2}% 51
\BOOKMARK [1][-]{section.6.1}{Continuation values objects with regression methods}{chapter.6}% 52
\BOOKMARK [2][-]{subsection.6.1.1}{Continuation values object}{section.6.1}% 53
\BOOKMARK [2][-]{subsection.6.1.2}{The GridAndRegressedValue object}{section.6.1}% 54
\BOOKMARK [2][-]{subsection.6.1.3}{The continuation cut object}{section.6.1}% 55
\BOOKMARK [1][-]{section.6.2}{Continuation objects and associated with trees}{chapter.6}% 56
\BOOKMARK [2][-]{subsection.6.2.1}{Continuation object}{section.6.2}% 57
\BOOKMARK [2][-]{subsection.6.2.2}{GridTreeValues}{section.6.2}% 58
\BOOKMARK [2][-]{subsection.6.2.3}{Continuation Cut with trees}{section.6.2}% 59
\BOOKMARK [-1][-]{part.3}{III Solving optimization problems with dynamic programming methods}{}% 60
\BOOKMARK [0][-]{chapter.7}{Creating simulators}{part.3}% 61
\BOOKMARK [1][-]{section.7.1}{Simulators for regression methods}{chapter.7}% 62
\BOOKMARK [1][-]{section.7.2}{Simulators for trees}{chapter.7}% 63
\BOOKMARK [0][-]{chapter.8}{Using conditional expectation to solve simple problems}{part.3}% 64
\BOOKMARK [1][-]{section.8.1}{American option by regression}{chapter.8}% 65
\BOOKMARK [2][-]{subsection.8.1.1}{The American option valuing by Longstaff\205Schwartz}{section.8.1}% 66
\BOOKMARK [1][-]{section.8.2}{American options by tree}{chapter.8}% 67
\BOOKMARK [2][-]{subsection.8.2.1}{The American option by tree}{section.8.2}% 68
\BOOKMARK [2][-]{subsection.8.2.2}{Python API}{section.8.2}% 69
\BOOKMARK [0][-]{chapter.9}{Using the general framework to manage stock problems }{part.3}% 70
\BOOKMARK [1][-]{section.9.1}{General requirement about business object}{chapter.9}% 71
\BOOKMARK [1][-]{section.9.2}{Solving the problem using conditional expectation calculated by regressions}{chapter.9}% 72
\BOOKMARK [2][-]{subsection.9.2.1}{Requirement to use the framework }{section.9.2}% 73
\BOOKMARK [2][-]{subsection.9.2.2}{Classical regression}{section.9.2}% 74
\BOOKMARK [2][-]{subsection.9.2.3}{Regressions and cuts for linear continuous transition problems with some concavity, convexity features}{section.9.2}% 75
\BOOKMARK [1][-]{section.9.3}{Solving the problem for Xx,t2 stochastic}{chapter.9}% 76
\BOOKMARK [2][-]{subsection.9.3.1}{Requirement to use the framework }{section.9.3}% 77
\BOOKMARK [2][-]{subsection.9.3.2}{The framework in optimization}{section.9.3}% 78
\BOOKMARK [2][-]{subsection.9.3.3}{The framework in simulation}{section.9.3}% 79
\BOOKMARK [1][-]{section.9.4}{Solving stock problems with trees}{chapter.9}% 80
\BOOKMARK [2][-]{subsection.9.4.1}{Solving dynamic programming problems with control discretization}{section.9.4}% 81
\BOOKMARK [2][-]{subsection.9.4.2}{Solving Dynamic Programming by solving LP problems}{section.9.4}% 82
\BOOKMARK [0][-]{chapter.10}{The Python API}{part.3}% 83
\BOOKMARK [1][-]{section.10.1}{Mapping to the framework}{chapter.10}% 84
\BOOKMARK [1][-]{section.10.2}{Special python binding}{chapter.10}% 85
\BOOKMARK [2][-]{subsection.10.2.1}{A first binding to use the framework}{section.10.2}% 86
\BOOKMARK [2][-]{subsection.10.2.2}{Binding to store/read a regressor and some two dimensional array}{section.10.2}% 87
\BOOKMARK [0][-]{chapter.11}{Using the C++ framework to solve some hedging problem}{part.3}% 88
\BOOKMARK [1][-]{section.11.1}{The problem}{chapter.11}% 89
\BOOKMARK [1][-]{section.11.2}{Theoretical algorithm}{chapter.11}% 90
\BOOKMARK [1][-]{section.11.3}{Practical algorithm based on Algorithm 8}{chapter.11}% 91
\BOOKMARK [-1][-]{part.4}{IV Semi-Lagrangian methods}{}% 92
\BOOKMARK [0][-]{chapter.12}{Theoretical background}{part.4}% 93
\BOOKMARK [1][-]{section.12.1}{Notation and regularity results}{chapter.12}% 94
\BOOKMARK [1][-]{section.12.2}{Time discretization for HJB equation}{chapter.12}% 95
\BOOKMARK [1][-]{section.12.3}{Space interpolation}{chapter.12}% 96
\BOOKMARK [0][-]{chapter.13}{C++ API}{part.4}% 97
\BOOKMARK [1][-]{section.13.1}{PDE resolution }{chapter.13}% 98
\BOOKMARK [1][-]{section.13.2}{Simulation framework}{chapter.13}% 99
\BOOKMARK [-1][-]{part.5}{V An example with both dynamic programming with regression and PDE}{}% 100
\BOOKMARK [0][-]{section.13.3}{The dynamic programming with regression approach}{part.5}% 101
\BOOKMARK [1][-]{section.13.4}{The PDE approach}{section.13.3}% 102
\BOOKMARK [-1][-]{part.6}{VI Stochastic Dual Dynamic Programming}{}% 103
\BOOKMARK [0][-]{chapter.14}{SDDP algorithm}{part.6}% 104
\BOOKMARK [1][-]{section.14.1}{Some general points about SDDP}{chapter.14}% 105
\BOOKMARK [1][-]{section.14.2}{A method, different algorithms}{chapter.14}% 106
\BOOKMARK [2][-]{subsection.14.2.1}{The basic case}{section.14.2}% 107
\BOOKMARK [2][-]{subsection.14.2.2}{Dependence of the random quantities}{section.14.2}% 108
\BOOKMARK [2][-]{subsection.14.2.3}{Non-convexity and conditional cuts}{section.14.2}% 109
\BOOKMARK [1][-]{section.14.3}{C++ API}{chapter.14}% 110
\BOOKMARK [2][-]{subsection.14.3.1}{Inputs }{section.14.3}% 111
\BOOKMARK [2][-]{subsection.14.3.2}{Architecture}{section.14.3}% 112
\BOOKMARK [2][-]{subsection.14.3.3}{Implement your problem}{section.14.3}% 113
\BOOKMARK [2][-]{subsection.14.3.4}{Set of parameters}{section.14.3}% 114
\BOOKMARK [2][-]{subsection.14.3.5}{The black box}{section.14.3}% 115
\BOOKMARK [2][-]{subsection.14.3.6}{Outputs }{section.14.3}% 116
\BOOKMARK [1][-]{section.14.4}{Python API \(only for regression based methods\)}{chapter.14}% 117
\BOOKMARK [-1][-]{part.7}{VII Nesting Monte Carlo for general non linear PDEs}{}% 118
\BOOKMARK [-1][-]{part.8}{VIII Some test cases description}{}% 119
\BOOKMARK [0][-]{chapter.15}{Some test cases description in C++}{part.8}% 120
\BOOKMARK [1][-]{section.15.1}{American option}{chapter.15}% 121
\BOOKMARK [2][-]{subsection.15.1.1}{testAmerican}{section.15.1}% 122
\BOOKMARK [2][-]{subsection.15.1.2}{testAmericanConvex}{section.15.1}% 123
\BOOKMARK [2][-]{subsection.15.1.3}{testAmericanForSparse}{section.15.1}% 124
\BOOKMARK [2][-]{subsection.15.1.4}{testAmericanOptionCorrel}{section.15.1}% 125
\BOOKMARK [2][-]{subsection.15.1.5}{testAmericanOptionTree}{section.15.1}% 126
\BOOKMARK [1][-]{section.15.2}{testSwingOption}{chapter.15}% 127
\BOOKMARK [2][-]{subsection.15.2.1}{testSwingOption2D}{section.15.2}% 128
\BOOKMARK [2][-]{subsection.15.2.2}{testSwingOption3}{section.15.2}% 129
\BOOKMARK [2][-]{subsection.15.2.3}{testSwingOptimSimu / testSwingOptimSimuMpi}{section.15.2}% 130
\BOOKMARK [2][-]{subsection.15.2.4}{testSwingOptimSimuWithHedge}{section.15.2}% 131
\BOOKMARK [2][-]{subsection.15.2.5}{testSwingOptimSimuND / testSwingOptimSimuNDMpi}{section.15.2}% 132
\BOOKMARK [1][-]{section.15.3}{Gas Storage}{chapter.15}% 133
\BOOKMARK [2][-]{subsection.15.3.1}{testGasStorage / testGasStorageMpi}{section.15.3}% 134
\BOOKMARK [2][-]{subsection.15.3.2}{testGasStorageCut / testGasStorageCutMpi}{section.15.3}% 135
\BOOKMARK [2][-]{subsection.15.3.3}{testGasStorageTree/testGasStorageTreeMpi}{section.15.3}% 136
\BOOKMARK [2][-]{subsection.15.3.4}{testGasStorageTreeCut/testGasStorageTreeCutMpi}{section.15.3}% 137
\BOOKMARK [2][-]{subsection.15.3.5}{testGasStorageKernel}{section.15.3}% 138
\BOOKMARK [2][-]{subsection.15.3.6}{testGasStorageVaryingCavity}{section.15.3}% 139
\BOOKMARK [2][-]{subsection.15.3.7}{testGasStorageSwitchingCostMpi}{section.15.3}% 140
\BOOKMARK [2][-]{subsection.15.3.8}{testGasStorageSDDP}{section.15.3}% 141
\BOOKMARK [2][-]{subsection.15.3.9}{testGasStorageSDDPTree}{section.15.3}% 142
\BOOKMARK [1][-]{section.15.4}{testLake / testLakeMpi}{chapter.15}% 143
\BOOKMARK [1][-]{section.15.5}{testOptionNIGL2}{chapter.15}% 144
\BOOKMARK [1][-]{section.15.6}{testDemandSDDP}{chapter.15}% 145
\BOOKMARK [1][-]{section.15.7}{Reservoir variations with SDDP}{chapter.15}% 146
\BOOKMARK [2][-]{subsection.15.7.1}{testReservoirWithInflowsSDDP}{section.15.7}% 147
\BOOKMARK [2][-]{subsection.15.7.2}{testStorageWithInflowsSDDP}{section.15.7}% 148
\BOOKMARK [2][-]{subsection.15.7.3}{testStorageWithInflowsAndMarketSDDP}{section.15.7}% 149
\BOOKMARK [1][-]{section.15.8}{Semi-Lagrangian}{chapter.15}% 150
\BOOKMARK [2][-]{subsection.15.8.1}{testSemiLagrangCase1/testSemiLagrangCase1}{section.15.8}% 151
\BOOKMARK [2][-]{subsection.15.8.2}{testSemiLagrangCase2/testSemiLagrangCase2}{section.15.8}% 152
\BOOKMARK [2][-]{subsection.15.8.3}{testSemiLagrangCase2/testSemiLagrangCase2}{section.15.8}% 153
\BOOKMARK [1][-]{section.15.9}{Non emimissive test case}{chapter.15}% 154
\BOOKMARK [2][-]{subsection.15.9.1}{testDPNonEmissive}{section.15.9}% 155
\BOOKMARK [2][-]{subsection.15.9.2}{testSLNonEmissive}{section.15.9}% 156
\BOOKMARK [1][-]{section.15.10}{Nesting for Non Linear PDE's}{chapter.15}% 157
\BOOKMARK [2][-]{subsection.15.10.1}{Some HJB test}{section.15.10}% 158
\BOOKMARK [2][-]{subsection.15.10.2}{Some Toy example: testUD2UTou}{section.15.10}% 159
\BOOKMARK [2][-]{subsection.15.10.3}{Some Portfolio optimization}{section.15.10}% 160
\BOOKMARK [0][-]{chapter.16}{Some python test cases description}{part.8}% 161
\BOOKMARK [1][-]{section.16.1}{Microgrid Management}{chapter.16}% 162
\BOOKMARK [2][-]{subsection.16.1.1}{testMicrogridBangBang}{section.16.1}% 163
\BOOKMARK [2][-]{subsection.16.1.2}{testMicrogrid}{section.16.1}% 164
\BOOKMARK [1][-]{section.16.2}{Dynamic Emulation Algorithm \(DEA\)}{chapter.16}% 165
\BOOKMARK [2][-]{subsection.16.2.1}{testMicrogridDEA}{section.16.2}% 166
