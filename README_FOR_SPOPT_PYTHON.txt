The whole library with headers, examples and python libs can be installed with the provided packages.
Windows :
*********
To use the StOpt library on windows  with the python interface without compiling the library.
- Install Python. Either python 2.7 or python 3.6 and numpy. We recommend to use anaconda for a single installation :
       https://www.continuum.io/downloads
- download the exe corresponding to the python version and install it for example on D:\StOptPython
- Modify the PATH environnement to add D:\StOptPython\lib and D:\StOptPython\lib\bin
- Modify the PYTHONPATH environnement to add D:\StOptPython\lib
Debian :
********
A package  StOpt is  provided for installation  for debian with  python 2.7. Please check compatibility with your kernel.
This package was generated with ubuntu zesty.
Install it by :

dpkg -i  StOptPackage.deb

