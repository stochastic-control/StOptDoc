\contentsline {part}{I\hspace {1em}Introduction}{6}{part.1}
\contentsline {chapter}{\numberline {1}General context}{7}{chapter.1}
\contentsline {chapter}{\numberline {2}General mathematical setting}{10}{chapter.2}
\contentsline {part}{II\hspace {1em}Useful tools for stochastic control}{12}{part.2}
\contentsline {chapter}{\numberline {3}The grids and their interpolators}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Linear grids}{17}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Definition and C++ API}{17}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}The python API}{19}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Legendre grids}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Approximation of a function in 1 dimension}{21}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Extension in dimension $d$}{24}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Troncature}{25}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}The C++ API}{26}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}The python API}{28}{subsection.3.2.5}
\contentsline {section}{\numberline {3.3}Sparse grids}{29}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}The linear sparse grid method}{29}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}High order sparse grid methods}{33}{section.3.4}
\contentsline {section}{\numberline {3.5}Anisotropy}{35}{section.3.5}
\contentsline {section}{\numberline {3.6}Adaptation}{35}{section.3.6}
\contentsline {section}{\numberline {3.7}C++ API}{37}{section.3.7}
\contentsline {section}{\numberline {3.8}Python API}{42}{section.3.8}
\contentsline {chapter}{\numberline {4}Introducing the regression resolution}{45}{chapter.4}
\contentsline {section}{\numberline {4.1}C++ global API}{46}{section.4.1}
\contentsline {section}{\numberline {4.2}Adapted local polynomial basis with same probability}{51}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Description of the method}{51}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}C++ API}{52}{subsection.4.2.2}
\contentsline {subsubsection}{The constant per cell approximation}{52}{section*.18}
\contentsline {subsubsection}{The linear per cell approximation}{53}{section*.19}
\contentsline {subsubsection}{An example in the linear case}{54}{section*.20}
\contentsline {subsection}{\numberline {4.2.3}Python API}{55}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Adapted local basis by K-Means clustering methods}{55}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}C++ API}{57}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Python api}{58}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Local polynomial basis with meshes of same size}{58}{section.4.4}
\contentsline {section}{\numberline {4.5}C++ API}{58}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}The constant per cell approximation}{58}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}The linear per cell approximation}{59}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}An example in the linear case}{59}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}Python API}{60}{subsection.4.5.4}
\contentsline {section}{\numberline {4.6}Sparse grid regressor}{60}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}C++ API}{60}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Python API}{61}{subsection.4.6.2}
\contentsline {section}{\numberline {4.7}Global polynomial basis}{61}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Description of the method}{61}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}C++ API}{62}{subsection.4.7.2}
\contentsline {subsection}{\numberline {4.7.3}Python API}{63}{subsection.4.7.3}
\contentsline {section}{\numberline {4.8}Kernel regression}{63}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}The univariate case}{64}{subsection.4.8.1}
\contentsline {subsection}{\numberline {4.8.2}The multivariate case}{65}{subsection.4.8.2}
\contentsline {subsubsection}{Kernel development}{66}{section*.22}
\contentsline {subsubsection}{Data partition}{66}{section*.23}
\contentsline {subsubsection}{Fast multivariate sweeping algorithm}{69}{section*.26}
\contentsline {subsection}{\numberline {4.8.3}C++ API}{70}{subsection.4.8.3}
\contentsline {subsection}{\numberline {4.8.4}Python API}{71}{subsection.4.8.4}
\contentsline {chapter}{\numberline {5}Calculating conditional expectation by trees}{72}{chapter.5}
\contentsline {subsection}{\numberline {5.0.1}C++ API}{73}{subsection.5.0.1}
\contentsline {subsubsection}{Calculating conditional expectation}{73}{section*.29}
\contentsline {subsubsection}{Python API}{74}{section*.30}
\contentsline {chapter}{\numberline {6}Continuation values objects and similar ones}{75}{chapter.6}
\contentsline {section}{\numberline {6.1}Continuation values objects with regression methods}{75}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Continuation values object}{75}{subsection.6.1.1}
\contentsline {subsubsection}{C++ API}{76}{section*.31}
\contentsline {subsubsection}{Python API}{78}{section*.32}
\contentsline {subsection}{\numberline {6.1.2}The GridAndRegressedValue object}{79}{subsection.6.1.2}
\contentsline {subsubsection}{C++ API}{79}{section*.33}
\contentsline {subsubsection}{Python API}{80}{section*.34}
\contentsline {subsection}{\numberline {6.1.3}The continuation cut object}{80}{subsection.6.1.3}
\contentsline {subsubsection}{C++ API}{81}{section*.36}
\contentsline {subsubsection}{Python API}{83}{section*.37}
\contentsline {section}{\numberline {6.2}Continuation objects and associated with trees}{84}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Continuation object}{84}{subsection.6.2.1}
\contentsline {subsubsection}{C++ API}{84}{section*.38}
\contentsline {subsubsection}{Python API}{85}{section*.39}
\contentsline {subsection}{\numberline {6.2.2}GridTreeValues}{85}{subsection.6.2.2}
\contentsline {subsubsection}{Python API}{86}{section*.40}
\contentsline {subsection}{\numberline {6.2.3}Continuation Cut with trees}{86}{subsection.6.2.3}
\contentsline {subsubsection}{C++ API}{86}{section*.41}
\contentsline {subsubsection}{Python API}{87}{section*.42}
\contentsline {part}{III\hspace {1em}Solving optimization problems with dynamic programming methods}{88}{part.3}
\contentsline {chapter}{\numberline {7}Creating simulators}{89}{chapter.7}
\contentsline {section}{\numberline {7.1}Simulators for regression methods}{89}{section.7.1}
\contentsline {section}{\numberline {7.2}Simulators for trees}{91}{section.7.2}
\contentsline {chapter}{\numberline {8}Using conditional expectation to solve simple problems}{95}{chapter.8}
\contentsline {section}{\numberline {8.1}American option by regression}{95}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}The American option valuing by Longstaff--Schwartz}{95}{subsection.8.1.1}
\contentsline {subsubsection}{American option by regression with the C++ API}{96}{section*.43}
\contentsline {subsubsection}{American option with the Python API}{96}{section*.44}
\contentsline {section}{\numberline {8.2}American options by tree}{97}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}The American option by tree}{97}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Python API}{97}{subsection.8.2.2}
\contentsline {chapter}{\numberline {9}Using the general framework to manage stock problems }{99}{chapter.9}
\contentsline {section}{\numberline {9.1}General requirement about business object}{100}{section.9.1}
\contentsline {section}{\numberline {9.2}Solving the problem using conditional expectation calculated by regressions}{102}{section.9.2}
\contentsline {subsection}{\numberline {9.2.1}Requirement to use the framework }{102}{subsection.9.2.1}
\contentsline {subsection}{\numberline {9.2.2}Classical regression}{103}{subsection.9.2.2}
\contentsline {subsubsection}{The framework in optimization with classical regressions}{105}{section*.46}
\contentsline {subsubsection}{The framework in simulation with classical regressions}{108}{section*.48}
\contentsline {subsection}{\numberline {9.2.3}Regressions and cuts for linear continuous transition problems with some concavity, convexity features}{115}{subsection.9.2.3}
\contentsline {subsubsection}{The framework in optimization using some cuts methods}{117}{section*.50}
\contentsline {subsubsection}{The framework in simulation using cuts to approximate the Bellman values}{119}{section*.51}
\contentsline {section}{\numberline {9.3}Solving the problem for $X^{x,t}_2$ stochastic}{122}{section.9.3}
\contentsline {subsection}{\numberline {9.3.1}Requirement to use the framework }{122}{subsection.9.3.1}
\contentsline {subsection}{\numberline {9.3.2}The framework in optimization}{124}{subsection.9.3.2}
\contentsline {subsection}{\numberline {9.3.3}The framework in simulation}{127}{subsection.9.3.3}
\contentsline {section}{\numberline {9.4}Solving stock problems with trees}{127}{section.9.4}
\contentsline {subsection}{\numberline {9.4.1}Solving dynamic programming problems with control discretization}{127}{subsection.9.4.1}
\contentsline {subsubsection}{Requirement to use the framework }{127}{section*.52}
\contentsline {subsubsection}{The framework}{130}{section*.53}
\contentsline {subsection}{\numberline {9.4.2}Solving Dynamic Programming by solving LP problems}{133}{subsection.9.4.2}
\contentsline {subsubsection}{Requirement to use the framework}{133}{section*.54}
\contentsline {subsubsection}{The framework in optimization using some cuts methods}{135}{section*.55}
\contentsline {subsubsection}{Using the framework in simulation}{137}{section*.56}
\contentsline {chapter}{\numberline {10}The Python API}{140}{chapter.10}
\contentsline {section}{\numberline {10.1}Mapping to the framework}{140}{section.10.1}
\contentsline {section}{\numberline {10.2}Special python binding}{145}{section.10.2}
\contentsline {subsection}{\numberline {10.2.1}A first binding to use the framework}{145}{subsection.10.2.1}
\contentsline {subsection}{\numberline {10.2.2}Binding to store/read a regressor and some two dimensional array}{148}{subsection.10.2.2}
\contentsline {chapter}{\numberline {11}Using the C++ framework to solve some hedging problem}{150}{chapter.11}
\contentsline {section}{\numberline {11.1}The problem}{150}{section.11.1}
\contentsline {section}{\numberline {11.2}Theoretical algorithm}{151}{section.11.2}
\contentsline {section}{\numberline {11.3}Practical algorithm based on Algorithm \ref {algoMeanVar3}}{154}{section.11.3}
\contentsline {part}{IV\hspace {1em}Semi-Lagrangian methods}{157}{part.4}
\contentsline {chapter}{\numberline {12}Theoretical background}{159}{chapter.12}
\contentsline {section}{\numberline {12.1}Notation and regularity results}{159}{section.12.1}
\contentsline {section}{\numberline {12.2}Time discretization for HJB equation}{160}{section.12.2}
\contentsline {section}{\numberline {12.3}Space interpolation}{160}{section.12.3}
\contentsline {chapter}{\numberline {13}C++ API}{162}{chapter.13}
\contentsline {section}{\numberline {13.1}PDE resolution }{168}{section.13.1}
\contentsline {section}{\numberline {13.2}Simulation framework}{170}{section.13.2}
\contentsline {part}{V\hspace {1em}An example with both dynamic programming with regression and PDE}{176}{part.5}
\contentsline {section}{\numberline {13.3}The dynamic programming with regression approach}{178}{section.13.3}
\contentsline {section}{\numberline {13.4}The PDE approach}{181}{section.13.4}
\contentsline {part}{VI\hspace {1em}Stochastic Dual Dynamic Programming}{184}{part.6}
\contentsline {chapter}{\numberline {14}SDDP algorithm}{185}{chapter.14}
\contentsline {section}{\numberline {14.1}Some general points about SDDP}{185}{section.14.1}
\contentsline {paragraph}{Notations used\\}{185}{section*.59}
\contentsline {paragraph}{Decision process \\}{185}{section*.60}
\contentsline {paragraph}{Dynamic programming principle \\}{186}{section*.61}
\contentsline {paragraph}{SDDP algorithm \\}{186}{section*.62}
\contentsline {section}{\numberline {14.2}A method, different algorithms}{187}{section.14.2}
\contentsline {paragraph}{Notations \\}{187}{section*.63}
\contentsline {subsection}{\numberline {14.2.1}The basic case}{187}{subsection.14.2.1}
\contentsline {paragraph}{Initialization \\}{188}{section*.64}
\contentsline {paragraph}{Forward pass\\}{188}{section*.65}
\contentsline {paragraph}{Backward pass\\}{188}{section*.66}
\contentsline {paragraph}{Stopping test \\}{188}{section*.67}
\contentsline {subsection}{\numberline {14.2.2}Dependence of the random quantities}{189}{subsection.14.2.2}
\contentsline {subsection}{\numberline {14.2.3}Non-convexity and conditional cuts}{191}{subsection.14.2.3}
\contentsline {subsubsection}{A tree approach}{193}{section*.68}
\contentsline {subsubsection}{A regression based approach}{193}{section*.69}
\contentsline {paragraph}{Regression, stochastic dynamic programming and SDDP \\}{194}{section*.70}
\contentsline {section}{\numberline {14.3}C++ API}{197}{section.14.3}
\contentsline {subsection}{\numberline {14.3.1}Inputs \\}{197}{subsection.14.3.1}
\contentsline {subsection}{\numberline {14.3.2}Architecture}{202}{subsection.14.3.2}
\contentsline {subsection}{\numberline {14.3.3}Implement your problem}{202}{subsection.14.3.3}
\contentsline {subsubsection}{Implement your own \textbf {TransitionOptimizer} class}{203}{section*.71}
\contentsline {subsubsection}{Implement your own \textbf {Simulator} class}{206}{section*.72}
\contentsline {subsubsection}{A simulator for the regression based method}{206}{section*.73}
\contentsline {subsubsection}{A simulator for the tree approach}{207}{section*.74}
\contentsline {subsection}{\numberline {14.3.4}Set of parameters\\}{207}{subsection.14.3.4}
\contentsline {subsubsection}{Implementing some regression based method}{207}{section*.75}
\contentsline {subsubsection}{Implementing a tree based method}{208}{section*.76}
\contentsline {subsection}{\numberline {14.3.5}The black box\\}{209}{subsection.14.3.5}
\contentsline {subsection}{\numberline {14.3.6}Outputs \\}{209}{subsection.14.3.6}
\contentsline {section}{\numberline {14.4}Python API (only for regression based methods)}{210}{section.14.4}
\contentsline {part}{VII\hspace {1em}Nesting Monte Carlo for general non linear PDEs}{215}{part.7}
\contentsline {part}{VIII\hspace {1em}Some test cases description}{221}{part.8}
\contentsline {chapter}{\numberline {15}Some test cases description in C++}{222}{chapter.15}
\contentsline {section}{\numberline {15.1}American option}{222}{section.15.1}
\contentsline {subsection}{\numberline {15.1.1}testAmerican}{222}{subsection.15.1.1}
\contentsline {subsubsection}{testAmericanGridKernelConstBasket1D}{223}{section*.84}
\contentsline {subsubsection}{testAmericanGridKernelLinearBasket1D}{223}{section*.85}
\contentsline {subsubsection}{testAmericanGlobalBasket3D}{224}{section*.94}
\contentsline {subsection}{\numberline {15.1.2}testAmericanConvex}{224}{subsection.15.1.2}
\contentsline {subsection}{\numberline {15.1.3}testAmericanForSparse}{225}{subsection.15.1.3}
\contentsline {subsection}{\numberline {15.1.4}testAmericanOptionCorrel}{225}{subsection.15.1.4}
\contentsline {subsection}{\numberline {15.1.5}testAmericanOptionTree}{225}{subsection.15.1.5}
\contentsline {section}{\numberline {15.2}testSwingOption}{226}{section.15.2}
\contentsline {subsection}{\numberline {15.2.1}testSwingOption2D}{226}{subsection.15.2.1}
\contentsline {subsection}{\numberline {15.2.2}testSwingOption3}{227}{subsection.15.2.2}
\contentsline {subsection}{\numberline {15.2.3}testSwingOptimSimu / testSwingOptimSimuMpi}{227}{subsection.15.2.3}
\contentsline {subsection}{\numberline {15.2.4}testSwingOptimSimuWithHedge}{227}{subsection.15.2.4}
\contentsline {subsection}{\numberline {15.2.5}testSwingOptimSimuND / testSwingOptimSimuNDMpi}{227}{subsection.15.2.5}
\contentsline {section}{\numberline {15.3}Gas Storage}{228}{section.15.3}
\contentsline {subsection}{\numberline {15.3.1}testGasStorage / testGasStorageMpi}{228}{subsection.15.3.1}
\contentsline {subsection}{\numberline {15.3.2}testGasStorageCut / testGasStorageCutMpi}{229}{subsection.15.3.2}
\contentsline {subsubsection}{testSimpleStorageCut}{229}{section*.111}
\contentsline {subsubsection}{testSimpleStorageCutDist}{229}{section*.112}
\contentsline {subsubsection}{testSimpleStorageMultipleFileCutDist}{229}{section*.113}
\contentsline {subsection}{\numberline {15.3.3}testGasStorageTree/testGasStorageTreeMpi}{229}{subsection.15.3.3}
\contentsline {subsection}{\numberline {15.3.4}testGasStorageTreeCut/testGasStorageTreeCutMpi}{229}{subsection.15.3.4}
\contentsline {subsection}{\numberline {15.3.5}testGasStorageKernel}{230}{subsection.15.3.5}
\contentsline {subsubsection}{testSimpleStorageKernel}{230}{section*.114}
\contentsline {subsection}{\numberline {15.3.6}testGasStorageVaryingCavity}{230}{subsection.15.3.6}
\contentsline {subsection}{\numberline {15.3.7}testGasStorageSwitchingCostMpi}{230}{subsection.15.3.7}
\contentsline {subsection}{\numberline {15.3.8}testGasStorageSDDP}{231}{subsection.15.3.8}
\contentsline {subsection}{\numberline {15.3.9}testGasStorageSDDPTree}{231}{subsection.15.3.9}
\contentsline {subsubsection}{testSimpleStorageDeterministicCutTree}{231}{section*.118}
\contentsline {subsubsection}{testSimpleStorageCutTree}{231}{section*.119}
\contentsline {subsubsection}{testSimpleStorageSDDPTree1D1Step}{231}{section*.120}
\contentsline {section}{\numberline {15.4}testLake / testLakeMpi}{232}{section.15.4}
\contentsline {section}{\numberline {15.5}testOptionNIGL2}{232}{section.15.5}
\contentsline {section}{\numberline {15.6}testDemandSDDP}{232}{section.15.6}
\contentsline {section}{\numberline {15.7}Reservoir variations with SDDP}{233}{section.15.7}
\contentsline {subsection}{\numberline {15.7.1}testReservoirWithInflowsSDDP}{233}{subsection.15.7.1}
\contentsline {subsection}{\numberline {15.7.2}testStorageWithInflowsSDDP}{234}{subsection.15.7.2}
\contentsline {subsection}{\numberline {15.7.3}testStorageWithInflowsAndMarketSDDP}{234}{subsection.15.7.3}
\contentsline {section}{\numberline {15.8}Semi-Lagrangian}{235}{section.15.8}
\contentsline {subsection}{\numberline {15.8.1}testSemiLagrangCase1/testSemiLagrangCase1}{235}{subsection.15.8.1}
\contentsline {subsection}{\numberline {15.8.2}testSemiLagrangCase2/testSemiLagrangCase2}{236}{subsection.15.8.2}
\contentsline {subsection}{\numberline {15.8.3}testSemiLagrangCase2/testSemiLagrangCase2}{236}{subsection.15.8.3}
\contentsline {section}{\numberline {15.9}Non emimissive test case}{237}{section.15.9}
\contentsline {subsection}{\numberline {15.9.1}testDPNonEmissive}{237}{subsection.15.9.1}
\contentsline {subsection}{\numberline {15.9.2}testSLNonEmissive}{237}{subsection.15.9.2}
\contentsline {section}{\numberline {15.10}Nesting for Non Linear PDE's}{237}{section.15.10}
\contentsline {subsection}{\numberline {15.10.1}Some HJB test}{237}{subsection.15.10.1}
\contentsline {subsubsection}{testHJCConst}{237}{section*.153}
\contentsline {subsubsection}{testHJCExact}{238}{section*.154}
\contentsline {subsubsection}{testHJBEuler}{238}{section*.155}
\contentsline {subsection}{\numberline {15.10.2}Some Toy example: testUD2UTou}{238}{subsection.15.10.2}
\contentsline {subsection}{\numberline {15.10.3}Some Portfolio optimization}{238}{subsection.15.10.3}
\contentsline {subsubsection}{testPortfolioExact}{239}{section*.156}
\contentsline {subsubsection}{testPortfolioEuler}{239}{section*.157}
\contentsline {chapter}{\numberline {16}Some python test cases description}{240}{chapter.16}
\contentsline {section}{\numberline {16.1}Microgrid Management}{240}{section.16.1}
\contentsline {subsection}{\numberline {16.1.1}testMicrogridBangBang}{240}{subsection.16.1.1}
\contentsline {subsection}{\numberline {16.1.2}testMicrogrid}{240}{subsection.16.1.2}
\contentsline {section}{\numberline {16.2}Dynamic Emulation Algorithm (DEA)}{241}{section.16.2}
\contentsline {subsection}{\numberline {16.2.1}testMicrogridDEA}{241}{subsection.16.2.1}
